![Demo](img/demo.gif)

## How To Use

* Ensure you have Python 3 installed.
* `git clone git@github.com:hauzer/dota-hero-grid-generator.git`.
* `cd dota-hero-grid-generator`.
* `$PYTHON -m venv py`.
* `py/Scripts/python -m pip install --upgrade git+https://github.com/anthony-tuininga/cx_Freeze.git@master`.
* `py/Scripts/python -m pip install -r requirements.txt`.
* Point to Steam in the config file.
* Configure your grids.
* Run with `py/Scripts/python ./dota_hero_grid_generator.py`.

Data provided by [STRATZ](https://stratz.com).
